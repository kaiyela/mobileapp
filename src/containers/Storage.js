/**
 * Storage.js
 *
 * 关于库存管理的主页面
 */
'use strict'
/**
 * ## Imports
 *
 * Redux
 */
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


/**
 * Router
 */
import {Actions} from 'react-native-router-flux'

/**
 * The actions we need
 */
import * as authActions from '../reducers/auth/authActions'

/**
 * Navigation Bar
 */
import NavigationBar from 'react-native-navbar'

import {
 View,
 Text
} from 'react-native'
/**
 *   LoginRender
 */
import LoginRender from '../components/LoginRender'

/**
 * The necessary React
 */
import React from 'react'

const {
  LOGIN,
  REGISTER,
  FORGOT_PASSWORD
} = require('../lib/constants').default

/**
 * ## Redux boilerplate
 */

function mapStateToProps (state) {
  return {
    auth: state.auth,
    global: state.global
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch)
  }
}

function buttonPressHandler (signup, username, email, password) {
  signup(username, email, password)
}

/**
 * ### Translations
 */
var I18n = require('react-native-i18n')
import Translations from '../lib/Translations'
I18n.translations = Translations

let Storage = React.createClass({
  
  render () {
    var titleConfig = {
      title: '库存管理'
    }

    var leftButtonConfig = {
      title: '返回',
      handler: Actions.pop
    }
    let loginButtonText = ''
    /*let onButtonPress = buttonPressHandler.bind(null,
                                                this.props.actions.signup,
                                                this.props.auth.form.fields.username,
                                                this.props.auth.form.fields.email,
                                                this.props.auth.form.fields.password)*/
    return (
      <View>
       <View>
        <NavigationBar
          title={titleConfig}
          leftButton={leftButtonConfig} />
        <Text>{"库存管理主页"}</Text>
	  </View>
     </View>
    )
  }
})
export default connect(mapStateToProps, mapDispatchToProps)(Storage)
