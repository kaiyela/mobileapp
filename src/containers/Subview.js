/**
 * # Subview.js
 *
 *  This is called from main to demonstrate the back button
 *
 */
'use strict'
/*
 * ## Imports
 *
 * Imports from redux
 */
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

/**
 * Router
 */
import {Actions} from 'react-native-router-flux'

/**
 * Navigation Bar
 */
import NavigationBar from 'react-native-navbar'

/**
 * The necessary components from React
 */
import React , {Component} from 'react'
import
{
  StyleSheet,
  View,
  Text,
  TextInput,
  Picker,
  TouchableHighlight,
  Switch,
  Modal
}
from 'react-native'

let Item = Picker.Item

//var DateTimePicker = require('react-native-datetime');

/**DateTimePickerExample = React.createClass({
    getInitialState() {
        return {
            date: new Date(),
        }
    },
    showDatePicker() {
        var date = this.state.date;
        this.picker.showDatePicker(date, (d)=>{
            this.setState({date:d});
        });
    },
    showTimePicker() {
        var date = this.state.date;
        this.picker.showTimePicker(date, (d)=>{
            this.setState({date:d});
        });
    },
    showDateTimePicker() {
        var date = this.state.date;
        this.picker.showDateTimePicker(date, (d)=>{
            this.setState({date:d});
        });
    },
    render() {
        return (
            <View style={styles.container}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.date.toString()}
                </Text>
                <View style={{height:40}} />
                <Button onPress={this.showDatePicker}>showDatePicker</Button>
                <View style={{height:40}} />
                <Button onPress={this.showTimePicker}>showTimePicker</Button>
                <View style={{height:40}} />
                <Button onPress={this.showDateTimePicker}>showDateTimePicker</Button>
                <DateTimePicker ref={(picker)=>{this.picker=picker}}/>
            </View>
        );
    },
});*/

//import UserModal from '../components/UserModal'

/**
 * Use device options so we can reference the Version
 *
 */
import * as deviceActions from '../reducers/device/deviceActions'

/**
* ## Redux boilerplate
*/

/**
 *  Instead of including all app states via ...state
 *  You probably want to explicitly enumerate only those which Main.js will depend on.
 *
 */
function mapStateToProps (state) {
  return {
    deviceVersion: state.device.version
  }
};

/*
 * Bind all the actions in deviceActions
 */
function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(deviceActions, dispatch)
  }
}

var styles = StyleSheet.create({
  container: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginTop: 0,
    padding: 10
  },
  summary: {
    fontFamily: 'BodoniSvtyTwoITCTT-Book',
    fontSize: 18,
    fontWeight: 'bold'
  },
  signleline: {

  },
  picker: {
    width: 100
  },
  pickeKehu: {
    width: 500
  },
  button: {
    borderRadius: 5,
    flex: 1,
    height: 44,
    alignSelf: 'stretch',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    fontSize: 18,
    margin: 5,
    textAlign: 'left',
  },
})

// add modal component

class Button extends React.Component {
  state = {
    active: false,
  };

  _onHighlight = () => {
    this.setState({active: true});
  };

  _onUnhighlight = () => {
    this.setState({active: false});
  };

  render() {
    var colorStyle = {
      color: this.state.active ? '#fff' : '#000',
    };
    return (
      <TouchableHighlight
        onHideUnderlay={this._onUnhighlight}
        onPress={this.props.onPress}
        onShowUnderlay={this._onHighlight}
        style={[styles.button, this.props.style]}
        underlayColor="#a9d9d4">
          <Text style={[styles.buttonText, colorStyle]}>{this.props.children}</Text>
      </TouchableHighlight>
    );
  }
}

const supportedOrientationsPickerValues = [
  ['portrait'],
  ['landscape'],
  ['landscape-left'],
  ['portrait', 'landscape-right'],
  ['portrait', 'landscape'],
  [],
];

class ModalExample extends React.Component {
  state = {
    animationType: 'none',
    modalVisible: false,
    transparent: false,
    selectedSupportedOrientation: 0,
    currentOrientation: 'unknown',
  };

  _setModalVisible = (visible) => {
    this.setState({modalVisible: visible});
  };

  _setAnimationType = (type) => {
    this.setState({animationType: type});
  };

  _toggleTransparent = () => {
    this.setState({transparent: !this.state.transparent});
  };
/*
<View style={styles.row}>
          <Text style={styles.rowTitle}>Animation Type</Text>
          <Button onPress={this._setAnimationType.bind(this, 'none')} style={this.state.animationType === 'none' ? activeButtonStyle : {}}>
            none
          </Button>
          <Button onPress={this._setAnimationType.bind(this, 'slide')} style={this.state.animationType === 'slide' ? activeButtonStyle : {}}>
            slide
          </Button>
          <Button onPress={this._setAnimationType.bind(this, 'fade')} style={this.state.animationType === 'fade' ? activeButtonStyle : {}}>
            fade
          </Button>
        </View>

        <View style={styles.row}>
          <Text style={styles.rowTitle}>Transparent</Text>
          <Switch value={this.state.transparent} onValueChange={this._toggleTransparent} />
        </View>

<View>
          <Text style={styles.rowTitle}>Supported orientations</Text>
          <Picker
            selectedValue={this.state.selectedSupportedOrientation}
            onValueChange={(_, i) => this.setState({selectedSupportedOrientation: i})}
            itemStyle={styles.pickerItem}
            >
            <Item label="Portrait" value={0} />
            <Item label="Landscape" value={1} />
            <Item label="Landscape left" value={2} />
            <Item label="Portrait and landscape right" value={3} />
            <Item label="Portrait and landscape" value={4} />
            <Item label="Default supportedOrientations" value={5} />
          </Picker>
        </View>
*/
  render() {
    var modalBackgroundStyle = {
      backgroundColor: this.state.transparent ? 'rgba(0, 0, 0, 0.5)' : '#f5fcff',
    };
    var innerContainerTransparentStyle = this.state.transparent
      ? {backgroundColor: '#fff', padding: 20}
      : null;
    var activeButtonStyle = {
      backgroundColor: '#ddd'
    };
    var self = this;
    return (
      <View>
        <Modal
          animationType={this.state.animationType}
          transparent={this.state.transparent}
          visible={this.state.modalVisible}
          onRequestClose={() => this._setModalVisible(false)}
          supportedOrientations={supportedOrientationsPickerValues[this.state.selectedSupportedOrientation]}
          onOrientationChange={evt => this.setState({currentOrientation: evt.nativeEvent.orientation})}
          >
          <View style={[styles.container, modalBackgroundStyle]}>
            <View style={[styles.innerContainer, innerContainerTransparentStyle]}>
              <NavigationBar
          	title={{title: '销售明细'}}
                rightButton={{title:'完成', handler: () => this.setState({modalVisible: false})}} />
                
            </View>
          </View>
        </Modal>

        <Button onPress={this._setModalVisible.bind(this, true)}>
          销售明细
        </Button>
      </View>
    );
  }
}



/**
 * ### Translations
 */
var I18n = require('react-native-i18n')
import Translations from '../lib/Translations'
I18n.translations = Translations

/**
 * ## Subview class
 */
type SelectionExampleState = {
  selection: {
    start: number,
    end: number
  },
  value: string
}

//let Subview = React.createClass({
class Subview extends Component {
  
  //state = SelectionExampleState
  state = {
    selected1: 'key0',
    selected2: 'key1',
    selected3: 'key2',
    color: 'red',
    mode: Picker.MODE_DROPDOWN,
    active: ''
  }
  

  _textInput = ''

  constructor(props) {
    super(props)
  }

  updateText() {
	
  }

  changeMode = () => {
    const newMode = this.state.mode === Picker.MODE_DIALOG
        ? Picker.MODE_DROPDOWN
        : Picker.MODE_DIALOG;
    this.setState({mode: newMode});
  }

  onValueChange = (key: string, value: string) => {
    const newState = {};
    newState[key] = value;
    this.setState(newState);
  }

  onPress = () => {

  }
  
  render () {
    var titleConfig = {
      title: '记收入'
    }

    var leftButtonConfig = {
      title: '返回',
      handler: Actions.pop
    }

    var colorStyle = {
      color: this.state.active ? '#fff' : '#000',
    };
/* ney
<Text style={styles.summary}>{I18n.t('Subview.subview')} {I18n.t('App.version')}: {this.props.deviceVersion}
          </Text>
*/  
    return (
      <View>
        <NavigationBar
          title={titleConfig}
          leftButton={leftButtonConfig} />
        <View style={styles.container}>
          <View>
        <TextInput
          autoCapitalize="none"
	  autoFocus={false}
	  keyboardType="numeric"
          placeholder="请输入金额"
          autoCorrect={false}
          onFocus={() => this.updateText('onFocus')}
          onBlur={() => this.updateText('onBlur')}
          onChange={(event) => this.updateText(
            'onChange text: ' + event.nativeEvent.text
          )}
          onEndEditing={(event) => this.updateText(
            'onEndEditing text: ' + event.nativeEvent.text
          )}
          onSubmitEditing={(event) => this.updateText(
            'onSubmitEditing text: ' + event.nativeEvent.text
          )}
          style={styles.singleLine}
        />
        <Picker
            style={styles.picker}
            selectedValue={this.state.selected1}
            onValueChange={this.onValueChange.bind(this, 'selected1')}>
            <Item label="现金" value="key0" />
            <Item label="银行账户" value="key1" />
            <Item label="支付宝" value="key2" />
          </Picker>
	 <Picker
            style={styles.picker}
            selectedValue={this.state.selected0}
            onValueChange={this.onValueChange.bind(this, 'selected0')}>
            <Item label="已收" value="key0" />
            <Item label="未收" value="key1" />
          </Picker>
          <Text>{'选择客户'}</Text>
	  <Picker
            style={styles.pickKehu}
            selectedValue={this.state.selected2}
            onValueChange={this.onValueChange.bind(this, 'selected2')}>
            <Item label="黄磊" value="key0" />
            <Item label="力天物业公司" value="key1" />
            <Item label="好美家家具有限公司" value="key2" />
          </Picker>
          <Text>{'收入类型'}</Text>
	  <Picker
            style={styles.picker}
            selectedValue={this.state.selected3}
            onValueChange={this.onValueChange.bind(this, 'selected3')}>
            <Item label="销售商品" value="key0" />
            <Item label="提供劳务" value="key1" />
            <Item label="预付款" value="key2" />
          </Picker>
          <ModalExample />
          <TouchableHighlight
        	underlayColor="#a9d9d4"
                style={styles.button}>
          	<Text style={[styles.buttonText]}>{'保存再记'}</Text>
      	  </TouchableHighlight>
          <TouchableHighlight
        	underlayColor="#a9d9d4"
                style={styles.button}>
          	<Text style={[styles.buttonText]}>{'保存'}</Text>
      	  </TouchableHighlight>

      </View>
          
        </View>
      </View>
    )
  }
//})
};
/**
 * Connect the properties
 */
export default connect(mapStateToProps, mapDispatchToProps)(Subview)
